from sklearn.metrics import brier_score_loss
#from sklearn.metrics.pairwise import euclidean_distances
from scipy.spatial.distance import euclidean
from scipy.stats import entropy
import numpy as np


# Just to be sure =)
def bs(f, o):
    sum = 0
    for i in range (0, len(f)):
        sum += (f[i] - o[i])**2
    return sum / len(f)


# Caso C - Scenario 1 
print ("Scenario 1")
y_true_categorical = np.array(["Positive", "Neutral", "Negative"])
# WSA
y_true = np.array([1, 0, 0])
y_prob = np.array([0.87125,0.12875,0])
print("WSA (BrierScore): ", brier_score_loss(y_true, y_prob))
print("WSA (Euclidean Distance): ", euclidean(y_true, y_prob))
print("WSA (KL-devergence)", entropy(y_true, y_prob))

#RNM
y_prob = np.array([0.99644078,0.0035592081,1.2687857e-08])
print("RNM (BrierScore): ", brier_score_loss(y_true, y_prob))
print("RNM (Euclidean Distance): ", euclidean(y_true, y_prob))
print("RNM (KL-devergence)", entropy(y_true, y_prob))


# Caso C - Scenario 2 
print ("\nScenario 2")
# WSA
y_true = np.array([0, 0, 1])
y_prob = np.array([0.16847259,0.20877017,0.62275723])
print("WSA (BrierScore): ", brier_score_loss(y_true, y_prob))
print("WSA (Euclidean Distance): ", euclidean(y_true, y_prob))
print("WSA (KL-devergence)", entropy(y_true, y_prob))

#RNM
y_prob = np.array([0.48263426, 0.47722358, 0.040142164])
print("RNM (BrierScore): ", brier_score_loss(y_true, y_prob))
print("RNM (Euclidean Distance): ", euclidean(y_true, y_prob))
print("RNM (KL-devergence)", entropy(y_true, y_prob))


# Caso C - Scenario 3 
print ("\nScenario 3")
# WSA
y_true = np.array([1, 0, 0])
y_prob = np.array([0.56288187,0.14447187,0.29264625])
print("WSA (BrierScore): ", brier_score_loss(y_true, y_prob))
print("WSA (Euclidean Distance): ", euclidean(y_true, y_prob))
print("WSA (KL-devergence)", entropy(y_true, y_prob))

#RNM
y_prob = np.array([0.99987457, 0.0001254293, 0])
print("RNM (BrierScore): ", brier_score_loss(y_true, y_prob))
print("RNM (Euclidean Distance): ", euclidean(y_true, y_prob))
print("RNM (KL-devergence)", entropy(y_true, y_prob))


# Caso C - Scenario 4 
print ("\nScenario 4")
# WSA
y_true = np.array([0, 0, 1])
y_prob = np.array([0.39164911,0.2009617,0.40738918])
print("WSA (BrierScore): ", brier_score_loss(y_true, y_prob))
print("WSA (Euclidean Distance): ", euclidean(y_true, y_prob))
print("WSA (KL-devergence)", entropy(y_true, y_prob))

#RNM
y_prob = np.array([0.9621124, 0.037869685, 1.791887e-05])
print("RNM (BrierScore): ", brier_score_loss(y_true, y_prob))
print("RNM (Euclidean Distance): ", euclidean(y_true, y_prob))
print("RNM (KL-devergence)", entropy(y_true, y_prob))


# Caso C - Scenario 5 
print ("\nScenario 5")
# WSA
y_true = np.array([1, 0, 0])
y_prob = np.array([0.8279375,0.1484375,0.023625])
print("WSA (BrierScore): ", brier_score_loss(y_true, y_prob))
print("WSA (Euclidean Distance): ", euclidean(y_true, y_prob))
print("WSA (KL-devergence)", entropy(y_true, y_prob))

#RNM
y_prob = np.array([0.98246637, 0.017533576, 0.98246637])
print("RNM (BrierScore): ", brier_score_loss(y_true, y_prob))
print("RNM (Euclidean Distance): ", euclidean(y_true, y_prob))
print("RNM (KL-devergence)", entropy(y_true, y_prob))


# Caso C - Scenario 6 
print ("\nScenario 6")
# WSA
y_true = np.array([1, 0, 0])
y_prob = np.array([0.87125,0.12875,0])
print("WSA (BrierScore): ", brier_score_loss(y_true, y_prob))
print("WSA (Euclidean Distance): ", euclidean(y_true, y_prob))
print("WSA (KL-devergence)", entropy(y_true, y_prob))

#RNM
y_prob = np.array([0.99647319, 0.0035267964, 1.254293e-08])
print("RNM (BrierScore): ", brier_score_loss(y_true, y_prob))
print("RNM (Euclidean Distance): ", euclidean(y_true, y_prob))
print("RNM (KL-devergence)", entropy(y_true, y_prob))


# Caso C - Scenario 7 
print ("\nScenario 7")
# WSA
y_true = np.array([0, 0, 1])
y_prob = np.array([0.342225,0.22,0.437775])
print("WSA (BrierScore): ", brier_score_loss(y_true, y_prob))
print("WSA (Euclidean Distance): ", euclidean(y_true, y_prob))
print("WSA (KL-devergence)", entropy(y_true, y_prob))

#RNM
y_prob = np.array([0.60672415, 0.39150158, 0.00177427])
print("RNM (BrierScore): ", brier_score_loss(y_true, y_prob))
print("RNM (Euclidean Distance): ", euclidean(y_true, y_prob))
print("RNM (KL-devergence)", entropy(y_true, y_prob))


