from node import Node
from wsa import WSA
from util import CSV
# 3 estados 

potential_gen_bugs = Node('Potential_gen_bugs', ['Very Low', "Low", "Medium", "High", "Very High"])
overall_value = Node('overall_value', ['Very Low', "Low", "Medium", "High", "Very High"])
effort = Node('effort', ['Very Low', "Low", "Medium", "High", "Very High"])
business_value = Node('business_value', ['Very Low', "Low", "Medium", "High", "Very High"])
api = Node('api', ['Very Low', "Low", "Medium", "High", "Very High"])
complexity = Node('complexity', ['Very Low', "Low", "Medium", "High", "Very High"])
Flows = Node('Flows', ['Very Low', "Low", "Medium", "High", "Very High"])
Pre_conditions = Node('Pre_conditions', ['Very Low', "Low", "Medium", "High", "Very High"])
Test_Automation = Node('Test_Automation', ['Very Low', "Low", "Medium", "High", "Very High"])
MVP = Node('MVP', ['Very Low', "Low", "Medium", "High", "Very High"])
dependent_ucs = Node('dependent_ucs', ['Very Low', "Low", "Medium", "High", "Very High"])

potential_gen_bugs.add_parent(api, 0.35)
potential_gen_bugs.add_parent(complexity, 0.65)

potential_gen_bugs.npt.add_compatible([0,0], [1,0,0,0,0], 0)
potential_gen_bugs.npt.add_compatible([1,1], [0.2,0.8,0,0,0], 0)
potential_gen_bugs.npt.add_compatible([2,1], [0,0.45,0.5,0.05,0], 0)
potential_gen_bugs.npt.add_compatible([3,2], [0,0,0.6,0.35,0.05], 0)
potential_gen_bugs.npt.add_compatible([4,2], [0,0,0.6,0.3,0.1], 0)

potential_gen_bugs.npt.add_compatible([2, 0], [0.85,0.15,0,0,0], 1)
potential_gen_bugs.npt.add_compatible([2, 1], [0,0.45,0.5,0.05,0], 1)
potential_gen_bugs.npt.add_compatible([2, 2], [0,0.05,0.8,0.15,0], 1)
potential_gen_bugs.npt.add_compatible([2, 3], [0,0.45,0.5,0.05,0], 1)
potential_gen_bugs.npt.add_compatible([2, 4], [0.85,0.15,0,0,0], 1)

WSA.apply(potential_gen_bugs)

overall_value.add_parent(potential_gen_bugs, 0.35)
overall_value.add_parent(business_value, 0.5)
overall_value.add_parent(effort, 0.15)

overall_value.npt.add_compatible([4,2,3], [0,0.1,0.7,0.15,0.05], 0)
overall_value.npt.add_compatible([3,2,3], [0,0.2,0.7,0.1,0], 0)
overall_value.npt.add_compatible([2,2,1], [0,0,0.8,0.2,0], 0)
overall_value.npt.add_compatible([1,2,0], [0,0.1,0.75,0.15,0], 0)
overall_value.npt.add_compatible([0,2,0], [0,0.2,0.7,0.1,0], 0)

overall_value.npt.add_compatible([2,4,2], [0,0,0.1,0.9,0], 1)
overall_value.npt.add_compatible([2,3,2], [0,0,0.9,0.1,0], 1)
overall_value.npt.add_compatible([2,2,2], [0,0.8,0.2,0,0], 1)
overall_value.npt.add_compatible([2,1,2], [0.2,0.8,0,0,0], 1)
overall_value.npt.add_compatible([2,0,2], [0.8,0.2,0,0,0], 1)

overall_value.npt.add_compatible([3,2,4], [0,0.05,0.6,0.25,0.1], 2)
overall_value.npt.add_compatible([2,2,3], [0,0.05,0.7,0.25,0], 2)
overall_value.npt.add_compatible([2,2,2], [0,0.8,0.2,0,0], 2)
overall_value.npt.add_compatible([1,2,1], [0,0.15,0.75,0.1,0], 2)
overall_value.npt.add_compatible([0,2,0], [0,0.2,0.7,0.1,0], 2)

WSA.apply(overall_value)

effort.add_parent(Flows, 0.4)
effort.add_parent(Pre_conditions, 0.4)
effort.add_parent(Test_Automation, 0.2)

effort.npt.add_compatible([4,4,4], [0,0,0,0.1,0.9], 0)
effort.npt.add_compatible([3,3,3], [0,0,0,0.9,0.1], 0)
effort.npt.add_compatible([2,2,2], [0,0,0.8,0.2,0], 0)
effort.npt.add_compatible([1,1,1], [0.2,0.8,0,0,0], 0)
effort.npt.add_compatible([0,0,0], [0.8,0.2,0,0,0], 0)

effort.npt.add_compatible([4,4,4], [0,0,0,0.1,0.9], 1)
effort.npt.add_compatible([3,3,3], [0,0,0,0.9,0.1], 1)
effort.npt.add_compatible([2,2,2], [0,0,0.8,0.2,0], 1)
effort.npt.add_compatible([1,1,1], [0.2,0.8,0,0,0], 1)
effort.npt.add_compatible([0,0,0], [0.8,0.2,0,0,0], 1)

effort.npt.add_compatible([4,4,4], [0,0,0,0.1,0.9], 2)
effort.npt.add_compatible([3,3,3], [0,0,0,0.9,0.1], 2)
effort.npt.add_compatible([2,2,2], [0,0,0.8,0.2,0], 2)
effort.npt.add_compatible([1,1,1], [0.2,0.8,0,0,0], 2)
effort.npt.add_compatible([0,0,0], [0.8,0.2,0,0,0], 2)

WSA.apply(effort)

business_value.add_parent(MVP, 0.7)
business_value.add_parent(dependent_ucs, 0.3)

business_value.npt.add_compatible([4,2], [0,0,0.05,0.15,0.8], 0)
business_value.npt.add_compatible([3,2], [0,0,0.1,0.2,0.7], 0)
business_value.npt.add_compatible([2,2], [0, 0.15,0.7,0.15,0], 0)
business_value.npt.add_compatible([1,1], [0.2,0.8,0,0,0], 0)
business_value.npt.add_compatible([0,0], [0.8,0.2,0,0,0], 0)

business_value.npt.add_compatible([3,4], [0,0,0.05,0.2,0.75], 1)
business_value.npt.add_compatible([3,3], [0,0,0.1,0.2,0.7], 1)
business_value.npt.add_compatible([2,2], [0, 0.15,0.7,0.15,0], 1)
business_value.npt.add_compatible([2,1], [0,0.2,0.65,0.15,0], 1)
business_value.npt.add_compatible([2,0], [0,0.25,0.6,0.15,0], 1)

WSA.apply(business_value)
business_value.npt.print()
#CSV.write("temp.csv", a.npt)
