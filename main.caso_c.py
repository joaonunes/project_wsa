from node import Node
from wsa import WSA

Feature_has_implem_sensors = Node('Feature_has_implem_sensors', ["Negative Impact", "Neutral Impact", "Positive Impact"])
Feature_is_core = Node('Feature_is_core', ["Negative Impact", "Neutral Impact", "Positive Impact"])
Funct_part_of_core = Node('Funct_part_of_core', ["Negative Impact", "Neutral Impact", "Positive Impact"])
Imp_funct_for_cust = Node('Imp_funct_for_cust', ["Negative Impact", "Neutral Impact", "Positive Impact"])
Existing_artifcts_UI = Node('Existing_artifcts_UI', ["Negative Impact", "Neutral Impact", "Positive Impact"])
Part_func_alr_started = Node('Part_func_alr_started', ["Negative Impact", "Neutral Impact", "Positive Impact"])
Viabil_become_prod = Node('Viabil_become_prod', ["Negative Impact", "Neutral Impact", "Positive Impact"])
ROI = Node('ROI', ["Negative Impact", "Neutral Impact", "Positive Impact"])
Overall_Value = Node('Overall_Value', ["Negative Impact", "Neutral Impact", "Positive Impact"])
Pending_results_last_sprt = Node('Pending_results_last_sprt', ["Negative Impact", "Neutral Impact", "Positive Impact"])
Team_motivation = Node('Team_motivation', ["Negative Impact", "Neutral Impact", "Positive Impact"])
Learning_curve = Node('Learning_curve', ["Negative Impact", "Neutral Impact", "Positive Impact"])
Funct_complexity = Node('Funct_complexity', ["Negative Impact", "Neutral Impact", "Positive Impact"])
Developer_Perspective = Node('Developer_Perspective', ["Negative Impact", "Neutral Impact", "Positive Impact"])
Compliance_sprint_sched = Node('Compliance_sprint_sched', ["Negative Impact", "Neutral Impact", "Positive Impact"])
Sprint_Succsess = Node('Sprint_Succsess', ["Negative Impact", "Neutral Impact", "Positive Impact"])

Funct_part_of_core.add_parent(Feature_has_implem_sensors, 0.7)
Funct_part_of_core.add_parent(Feature_is_core, 0.3)


Funct_part_of_core.npt.add_compatible([2, 2], [0, 0, 1], 0)
Funct_part_of_core.npt.add_compatible([1, 1], [0, .9, .1], 0)
Funct_part_of_core.npt.add_compatible([0, 1], [0, .5, .5], 0)

Funct_part_of_core.npt.add_compatible([1, 2], [0, .2, .8], 1)
Funct_part_of_core.npt.add_compatible([1, 1], [0, .9, .1], 1)
Funct_part_of_core.npt.add_compatible([0, 0], [1, 0, 0], 1)

WSA.apply(Funct_part_of_core)

Viabil_become_prod.add_parent(Existing_artifcts_UI, 0.6)
Viabil_become_prod.add_parent(Part_func_alr_started, 0.4)

Viabil_become_prod.npt.add_compatible([2, 1], [0, 0.2, 0.8], 0)
Viabil_become_prod.npt.add_compatible([1, 1], [0, .9, .1], 0)
Viabil_become_prod.npt.add_compatible([0, 1], [0.9, .1, 0], 0)

Viabil_become_prod.npt.add_compatible([2, 2], [0, 0.05, .95], 1)
Viabil_become_prod.npt.add_compatible([1, 1], [0, .9, .1], 1)
Viabil_become_prod.npt.add_compatible([1, 0], [0, 1, 0], 1)

WSA.apply(Viabil_become_prod)

ROI.add_parent(Imp_funct_for_cust, 0.8)
ROI.add_parent(Viabil_become_prod, 0.2)

ROI.npt.add_compatible([2, 2], [0, 0, 1], 0)
ROI.npt.add_compatible([1, 1], [0, .9, .1], 0)
ROI.npt.add_compatible([0, 0], [1, 0, 0], 0)

ROI.npt.add_compatible([1, 2], [0, 0.3, .7], 1)
ROI.npt.add_compatible([1, 1], [0, .9, .1], 1)
ROI.npt.add_compatible([1, 0], [0.7, 0.3, 0], 1)

WSA.apply(ROI)

Sprint_Succsess.add_parent(Developer_Perspective, .35)
Sprint_Succsess.add_parent(Compliance_sprint_sched, 0.65)

Sprint_Succsess.npt.add_compatible([2, 2], [0, 0.2, .8], 0)
Sprint_Succsess.npt.add_compatible([1, 1], [0.1, .8, .1], 0)
Sprint_Succsess.npt.add_compatible([0, 0], [0.95, .05, 0], 0)

Sprint_Succsess.npt.add_compatible([2, 2], [0, 0.2, .8], 1)
Sprint_Succsess.npt.add_compatible([1, 1], [0.1, .8, .1], 1)
Sprint_Succsess.npt.add_compatible([0, 0], [0.95, 0.05, 0], 1)

WSA.apply(Sprint_Succsess)

Overall_Value.add_parent(ROI, 0.25)
Overall_Value.add_parent(Sprint_Succsess, 0.75)

Overall_Value.npt.add_compatible([2, 1], [0, 0.4, .6], 0)
Overall_Value.npt.add_compatible([1, 1], [0, .8, .2], 0)
Overall_Value.npt.add_compatible([0, 1], [0.8, .2, 0], 0)

Overall_Value.npt.add_compatible([1, 2], [0, 0.1, .9], 1)
Overall_Value.npt.add_compatible([1, 1], [0, .8, .2], 1)
Overall_Value.npt.add_compatible([1, 0], [0.9, 0.1, 0], 1)

WSA.apply(Overall_Value)


Developer_Perspective.add_parent(Team_motivation, 0.3)
Developer_Perspective.add_parent(Learning_curve, 0.7)

Developer_Perspective.npt.add_compatible([2, 1], [0, 0.2, .8], 0)
Developer_Perspective.npt.add_compatible([1, 1], [0, 1, 0], 0)
Developer_Perspective.npt.add_compatible([0, 0], [1, 0, 0], 0)

Developer_Perspective.npt.add_compatible([1, 2], [0, 0.1, .9], 1)
Developer_Perspective.npt.add_compatible([1, 1], [0, 1, 0], 1)
Developer_Perspective.npt.add_compatible([0, 0], [1, 0, 0], 1)

WSA.apply(Developer_Perspective)

Sprint_Succsess.npt.print()
print(len(Sprint_Succsess.npt.distributions))