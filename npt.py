from exceptions import NodeInvalidStateError

class NPTEntry:
    
    def __init__(self, evidences, evidences_as_int, state_values, is_compatible = False):
        self.evidences = evidences
        self.evidences_as_int = evidences_as_int
        self.state_values = state_values
        self.is_compatible = is_compatible


class NPT:

    def __init__(self, child):
        self.child = child
        self.compatible_parents = [] 
        self.distributions = []
    
    def apply_wsa(self):
        # why .99999999...? 
        # .3+.6+.1 == .9999999999999999 not 1
        if sum(self.child.parents_weight) < .98:
            raise NodeInvalidStateError("At: Applying WSA", "The sum of weights should be 1")

        for entry in self.distributions:
            if not entry.is_compatible:
                for state_of_interest in range (0, len(self.child.state_values)):
                    res = 0
                    for current_parent in range (0, len(self.child.parents)):
                        res += self.child.parents_weight[current_parent] * self.compatible_parents[current_parent][entry.evidences_as_int[current_parent]][state_of_interest]                            
                    entry.state_values[state_of_interest] = res

    def generate_empty_dist(self, evidences, evidences_as_int, empty_dist, parent_index):
        for state in self.child.parents[parent_index].state_names:
            evidences[parent_index] = state
            evidences_as_int[parent_index] = self.child.parents[parent_index].state_names.index(state)
            if parent_index + 1 == len(self.child.parents):
                c_evidences = evidences.copy()
                c_evidences_as_int = evidences_as_int.copy()
                c_empty_dist = empty_dist.copy()
                entry = NPTEntry(c_evidences, c_evidences_as_int, c_empty_dist)
                self.distributions.append(entry)
                if state == self.child.parents[parent_index].state_names[-1]:
                    return 0
            else:
                self.generate_empty_dist(evidences, evidences_as_int, empty_dist, parent_index + 1)

    def init_compatible_parents(self):
        self.compatible_parents = []
        for p in self.child.parents:
            x = []
            for v in p.state_values:
                x.append([])    
            self.compatible_parents.append(x)

    def init_distributions(self):
        self.distributions = []
        n_states = len(self.child.state_values)
        empty_dist = []
        for i in range(0, n_states):
            empty_dist.append(0)
        evidences = []
        evidences_as_int = []
        for i in range(0, len(self.child.parents)):
            evidences.append("")
            evidences_as_int.append(0)
        self.generate_empty_dist(evidences, evidences_as_int, empty_dist, 0)
    
    def init(self):
        self.init_compatible_parents()
        self.init_distributions()

    def get_npt_entry(self, evidences_as_int):
        for entry in self.distributions:
            if entry.evidences_as_int == evidences_as_int:
                return entry

    def add_compatible(self, evidence_as_int, state_values, parent_index):
        entry = self.get_npt_entry(evidence_as_int)
        entry.state_values = state_values
        entry.is_compatible = True
        self.compatible_parents[parent_index][evidence_as_int[parent_index]] = state_values

    def print(self):
        print ("{", end=" ")
        for entry in self.distributions:
            for v in entry.state_values:
                print (v, end=",")
        print("}")