from sklearn.metrics import brier_score_loss
#from sklearn.metrics.pairwise import euclidean_distances
from scipy.spatial.distance import euclidean
from scipy.stats import entropy
import numpy as np


# Just to be sure =)
def bs(f, o):
    sum = 0
    for i in range (0, len(f)):
        sum += (f[i] - o[i])**2
    return sum / len(f)


# Caso A - Scenario 1 
print ("Scenario 1")
y_true_categorical = np.array(["Positive", "Neutral", "Negative"])
# WSA
y_true = np.array([0, 0, 1])
y_prob = np.array([0.22516912,0.12442164,0.65040924])
print("WSA (BrierScore): ", brier_score_loss(y_true, y_prob))
print("WSA (Euclidean Distance): ", euclidean(y_true, y_prob))
print("WSA (KL-devergence)", entropy(y_true, y_prob))

#RNM
y_prob = np.array([0, 0.69942613, 0.30057387])
print("RNM (BrierScore): ", brier_score_loss(y_true, y_prob))
print("RNM (Euclidean Distance): ", euclidean(y_true, y_prob))
print("RNM (KL-devergence)", entropy(y_true, y_prob))


# Caso A - Scenario 2 
print ("\nScenario 2")
# WSA
y_true = np.array([0, 0, 1])
y_prob = np.array([0.41178771,0.38821229,0.2])
print("WSA (BrierScore): ", brier_score_loss(y_true, y_prob))
print("WSA (Euclidean Distance): ", euclidean(y_true, y_prob))
print("WSA (KL-devergence)", entropy(y_true, y_prob))

#RNM
y_prob = np.array([0, 0.73293906, 0.26706094])
print("RNM (BrierScore): ", brier_score_loss(y_true, y_prob))
print("RNM (Euclidean Distance): ", euclidean(y_true, y_prob))
print("RNM (KL-devergence)", entropy(y_true, y_prob))


# Caso A - Scenario 3 
print ("\nScenario 3")
# WSA
y_true = np.array([0, 0, 1])
y_prob = np.array([0.17029171,0.46657229,0.363136])
print("WSA (BrierScore): ", brier_score_loss(y_true, y_prob))
print("WSA (Euclidean Distance): ", euclidean(y_true, y_prob))
print("WSA (KL-devergence)", entropy(y_true, y_prob))

#RNM
y_prob = np.array([0, 0.95194206, 0.048057941])
print("RNM (BrierScore): ", brier_score_loss(y_true, y_prob))
print("RNM (Euclidean Distance): ", euclidean(y_true, y_prob))
print("RNM (KL-devergence)", entropy(y_true, y_prob))


# Caso A - Scenario 4 
print ("\nScenario 4")
# WSA
y_true = np.array([0, 0, 1])
y_prob = np.array([0.3170161,0.084978341,0.59800556])
print("WSA (BrierScore): ", brier_score_loss(y_true, y_prob))
print("WSA (Euclidean Distance): ", euclidean(y_true, y_prob))
print("WSA (KL-devergence)", entropy(y_true, y_prob))

#RNM
y_prob = np.array([0, 0.24828974, 0.75171026])
print("RNM (BrierScore): ", brier_score_loss(y_true, y_prob))
print("RNM (Euclidean Distance): ", euclidean(y_true, y_prob))
print("RNM (KL-devergence)", entropy(y_true, y_prob))


# Caso A - Scenario 5 
print ("\nScenario 5")
# WSA
y_true = np.array([1, 0, 0])
y_prob = np.array([0.65687198,0.11842871,0.22469931])
print("WSA (BrierScore): ", brier_score_loss(y_true, y_prob))
print("WSA (Euclidean Distance): ", euclidean(y_true, y_prob))
print("WSA (KL-devergence)", entropy(y_true, y_prob))

#RNM
y_prob = np.array([0.5498607, 0.4501393, 0])
print("RNM (BrierScore): ", brier_score_loss(y_true, y_prob))
print("RNM (Euclidean Distance): ", euclidean(y_true, y_prob))
print("RNM (KL-devergence)", entropy(y_true, y_prob))


# Caso A - Scenario 6 
print ("\nScenario 6")
# WSA
y_true = np.array([0, 0, 1])
y_prob = np.array([0.04507902,0.00480594,0.95011504])
print("WSA (BrierScore): ", brier_score_loss(y_true, y_prob))
print("WSA (Euclidean Distance): ", euclidean(y_true, y_prob))
print("WSA (KL-devergence)", entropy(y_true, y_prob))

#RNM
y_prob = np.array([0, 0, 1])
print("RNM (BrierScore): ", brier_score_loss(y_true, y_prob))
print("RNM (Euclidean Distance): ", euclidean(y_true, y_prob))
print("RNM (KL-devergence)", entropy(y_true, y_prob))


# Caso A - Scenario 7 
print ("\nScenario 7")
# WSA
y_true = np.array([1, 0, 0])
y_prob = np.array([0.65687198,0.11842871,0.22469931])
print("WSA (BrierScore): ", brier_score_loss(y_true, y_prob))
print("WSA (Euclidean Distance): ", euclidean(y_true, y_prob))
print("WSA (KL-devergence)", entropy(y_true, y_prob))

#RNM
y_prob = np.array([0.5498607, 0.4501393, 0])
print("RNM (BrierScore): ", brier_score_loss(y_true, y_prob))
print("RNM (Euclidean Distance): ", euclidean(y_true, y_prob))
print("RNM (KL-devergence)", entropy(y_true, y_prob))


# Caso A - Scenario 9 
print ("\nScenario 9")
# WSA
y_true = np.array([0, 0, 1])
y_prob = np.array([0.3231475,0.09012808,0.58672442])
print("WSA (BrierScore): ", brier_score_loss(y_true, y_prob))
print("WSA (Euclidean Distance): ", euclidean(y_true, y_prob))
print("WSA (KL-devergence)", entropy(y_true, y_prob))

#RNM
y_prob = np.array([0.14695674, 0.85304326, 0])
print("RNM (BrierScore): ", brier_score_loss(y_true, y_prob))
print("RNM (Euclidean Distance): ", euclidean(y_true, y_prob))
print("RNM (KL-devergence)", entropy(y_true, y_prob))