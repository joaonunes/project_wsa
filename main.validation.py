from node import Node
from wsa import WSA

# From DAS
a = Node('Node A', ["very low", "low", "mid", "high", "very high"])
b = Node('Node B', ["very low", "low", "mid", "high", "very high"])
c = Node('Node C', ["very low", "low", "mid", "high", "very high"])
d = Node('Node D', ["very low", "low", "mid", "high", "very high"])

a.add_parent(b, 0.1)
a.add_parent(c, 0.45)
a.add_parent(d, 0.45)

a.npt.add_compatible([0, 0, 0], [.8, .15, .03, .015, .005], 0)
a.npt.add_compatible([1, 1, 2], [.08, .8, .08, .03, .01], 0)
a.npt.add_compatible([2, 1, 1], [.02, .08, .8, .08, .02], 0)
a.npt.add_compatible([3, 1, 1], [.01, .03, .08, .8, .08], 0)
a.npt.add_compatible([4, 1, 1], [.005, .015, .03, .15, .8], 0)

a.npt.add_compatible([0, 0, 0], [.8, .15, .03, .015, .005], 1)
a.npt.add_compatible([1, 1, 2], [.08, .8, .08, .03, .01], 1)
a.npt.add_compatible([1, 2, 1], [.02, .08, .8, .08, .02], 1)
a.npt.add_compatible([1, 3, 1], [.01, .03, .08, .8, .08], 1)
a.npt.add_compatible([1, 4, 1], [.005, .015, .03, .15, .8], 1)

a.npt.add_compatible([0, 0, 0], [.8, .15, .03, .015, .005], 2)
a.npt.add_compatible([2, 1, 1], [.08, .8, .08, .03, .01], 2)
a.npt.add_compatible([1, 1, 2], [.02, .08, .8, .08, .02], 2)
a.npt.add_compatible([1, 1, 3], [.01, .03, .08, .8, .08], 2)
a.npt.add_compatible([1, 1, 4], [.005, .015, .03, .15, .8], 2)

WSA.apply(a)

