import csv
from npt import NPT, NPTEntry

class CSV:
    def __init__(self, *args, **kwargs):
        pass

    @staticmethod
    def write(filename, npt):
        with open(filename, 'w', newline='', encoding='utf-8') as f:
            writer = csv.reader(f)
            for entry in npt.distributions:
                for value in entry.state_values:
                    writer.writerow(['value'])    