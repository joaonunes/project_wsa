from node import Node
from wsa import WSA
from util import CSV
# 3 estados 

coesao = Node('Coesão', ["Low", "Medium", "High"]) # minimize effort
autogerenciamento = Node('Gerenciamento', ["Low", "Medium", "High"]) # minimize effort
colaboracao = Node('Colaboração', ["Low", "Medium", "High"]) # minimize effort

compartilhamento_lideranca = Node('Compartilhamento Liderença', ["Low", "Medium", "High"])
experiencia = Node('Experiência', ["Low", "Medium", "High"]) # minimize effort

orientacao_equipe = Node('Orientação da Equipe', ["Low", "Medium", "High"]) # minimize effort
coordenacao = Node('Coordenação', ["Low", "Medium", "High"]) # minimize effort


coesao.add_parent(autogerenciamento, .4)
coesao.add_parent(colaboracao, .6)

coesao.npt.add_compatible([2,2], [0,0,1], 0)
coesao.npt.add_compatible([1,2], [0,0.2,0.8], 0)
coesao.npt.add_compatible([0,0], [1,0,0], 0)

coesao.npt.add_compatible([2,2], [0,0,1], 1)
coesao.npt.add_compatible([1,1], [0,1,0], 1)
coesao.npt.add_compatible([0,0], [1,0,0], 1)


autogerenciamento.add_parent(compartilhamento_lideranca, .3)
autogerenciamento.add_parent(experiencia, .7)

autogerenciamento.npt.add_compatible([2,2], [0,0,1], 0)
autogerenciamento.npt.add_compatible([1,2], [0,0.1,0.9], 0)
autogerenciamento.npt.add_compatible([0,1], [.3,0.7,0], 0)

autogerenciamento.npt.add_compatible([2,2], [0,0,1], 1)
autogerenciamento.npt.add_compatible([1,1], [0,1,0], 1)
autogerenciamento.npt.add_compatible([0,0], [1,0,0], 1)

colaboracao.add_parent(orientacao_equipe, .4)
colaboracao.add_parent(coordenacao, 0.6)

colaboracao.npt.add_compatible([2,2], [0,0,1], 0)
colaboracao.npt.add_compatible([1,1], [0,1,0], 0)
colaboracao.npt.add_compatible([0,0], [1,0,0], 0)

colaboracao.npt.add_compatible([2,2], [0,0,1], 1)
colaboracao.npt.add_compatible([1,1], [0,1,0], 1)
colaboracao.npt.add_compatible([0,0], [1,0,0], 1)

WSA.apply(coesao)

coesao.npt.print()
print(len(coesao.npt.distributions))
