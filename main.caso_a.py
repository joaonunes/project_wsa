from node import Node
from wsa import WSA
from util import CSV
# 3 estados 

third_party = Node('Availability of third-party libraries', ["Positive Impact", "Neutral Impact", "Negative Impact"])
effort = Node('Effort', ["Low", "Medium", "High"]) # minimize effort
implementation_complexity = Node('Implementation Complexity', ["Positive Impact", "Neutral Impact", "Negative Impact"])

meets_requirements = Node('meets_requirements', ["Positive Impact", "Neutral Impact", "Negative Impact"])
reuse_potential = Node('reuse_potential', ["Positive Impact", "Neutral Impact", "Negative Impact"])
overall_value = Node('Overall Value', ["High", "Medium", "Low"])

usability = Node('usability', ["Low", "Medium", "High"])
colors_match = Node('colors_match', ["Positive Impact", "Neutral Impact", "Negative Impact"])
ui_simplicity = Node('ui_simplicity', ["Positive Impact", "Neutral Impact", "Negative Impact"])
meaningful_ui = Node('meaningful_ui', ["Positive Impact", "Neutral Impact", "Negative Impact"])
default_navigation = Node('default_navigation', ["Positive Impact", "Neutral Impact", "Negative Impact"])
not_many_items_ui = Node('not_many_items_ui', ["Positive Impact", "Neutral Impact", "Negative Impact"])
clicks_to_task = Node('clicks_to_task', ["Positive Impact", "Neutral Impact", "Negative Impact"])

# Compatible Parents and relationships
ui_simplicity.add_parent(clicks_to_task, 0.2)
ui_simplicity.add_parent(not_many_items_ui, 0.3)
ui_simplicity.add_parent(default_navigation, 0.5)

ui_simplicity.npt.add_compatible([0,0,2], [0,0.3,0.7], 0)
ui_simplicity.npt.add_compatible([1,1,0], [0.6,0.4,0], 0)
ui_simplicity.npt.add_compatible([2,2,2], [0,0,1], 0)

ui_simplicity.npt.add_compatible([1,0,0], [0.8,0.2,0], 1)
ui_simplicity.npt.add_compatible([0,1,0], [0.8,0.2,0], 1)
ui_simplicity.npt.add_compatible([2,2,0], [0,0.2,0.8], 1)

ui_simplicity.npt.add_compatible([0,0,0], [1,0,0], 2)
ui_simplicity.npt.add_compatible([2,2,1], [0,0.1,0.9], 2)
ui_simplicity.npt.add_compatible([0,2,2], [0,0,1], 2)

WSA.apply(ui_simplicity)

usability.add_parent(ui_simplicity, 0.3)
usability.add_parent(meaningful_ui, 0.6)
usability.add_parent(colors_match, 0.1)

usability.npt.add_compatible([0,2,0], [0.7,0.3,0], 0)
usability.npt.add_compatible([1,0,0], [0,0.5,0.5], 0)
usability.npt.add_compatible([2,2,0], [1,0,0], 0)

usability.npt.add_compatible([0,0,0], [0,0,1], 1)
usability.npt.add_compatible([2,1,0], [0.9,0.1,0], 1)
usability.npt.add_compatible([1,2,0], [0.9,0.1,0], 1)

usability.npt.add_compatible([2,0,0], [0.8,0.2,0], 2)
usability.npt.add_compatible([0,0,1], [0,0.2,0.8], 2)
usability.npt.add_compatible([0,0,2], [0.4,0.6,0], 2)

WSA.apply(usability)

# Minimize effort
effort.add_parent(third_party, 0.4)
effort.add_parent(implementation_complexity, 0.6)

effort.npt.add_compatible([0,2], [0,0.1,0.9], 0)
effort.npt.add_compatible([1,2], [0,0.4,0.6], 0)
effort.npt.add_compatible([2,0], [0,0,1], 0)

effort.npt.add_compatible([0,0], [0,0,1], 1)
effort.npt.add_compatible([2,1], [0,0.3,0.7], 1)
effort.npt.add_compatible([2,2], [1,0,0], 1)

WSA.apply(effort)

overall_value.add_parent(meets_requirements, 0.4)
overall_value.add_parent(usability, 0.2)
overall_value.add_parent(reuse_potential, 0.2)
overall_value.add_parent(effort, 0.2)

overall_value.npt.add_compatible([0,0,2,1], [0,0,1], 0)
overall_value.npt.add_compatible([1,2,0,2], [0.2,0.8,0], 0)
overall_value.npt.add_compatible([2,0,2,0], [0,0,1], 0)

overall_value.npt.add_compatible([0,0,0,1], [0.4,0.6,0], 1)
overall_value.npt.add_compatible([0,1,0,2], [0.9,0.1,0], 1)
overall_value.npt.add_compatible([0,2,0,2], [1,0,0], 1)

overall_value.npt.add_compatible([0,2,0,0], [0.6,0.4,0], 2)
overall_value.npt.add_compatible([2,1,1,0], [0,0,1], 2)
overall_value.npt.add_compatible([0,2,2,2], [0.9,0.1,0], 2)

overall_value.npt.add_compatible([0,0,2,0], [0,0,1], 3)
overall_value.npt.add_compatible([2,2,0,1], [0,0,1], 3)
overall_value.npt.add_compatible([0,0,2,2], [0,0.2,0.8], 3)

WSA.apply(overall_value)


overall_value.npt.print()
print(len(overall_value.npt.distributions))
