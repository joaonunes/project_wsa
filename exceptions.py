class Error(Exception):
    """Classe base para exceções neste módulo"""
    pass


class NodeInvalidStateError(Error):
    """
    Exceção para nodes com estados incompatíveis

    expression -- onde o erro ocorreu (o nó)
    message -- explicação do erro
    """
    def __init__(self, expression, message):
        self.expression = expression
        self.message = message