from npt import NPT, NPTEntry
from exceptions import NodeInvalidStateError
        
class Node:
    
    def __init__(self, title, state_names):
        self.title = title
        self.state_names = state_names
        self.state_values = []
        self.parents = []
        self.parents_weight = []
        self.children = []
        self.npt = NPT(self)
        self.evidence = ""
        self.evidence_as_int = -1

        number_of_states = len(self.state_names)
        for state in self.state_names:
            self.state_values.append(1/number_of_states)

        if len(self.parents) != len(self.parents_weight):
            raise NodeInvalidStateError("At: " + self.title + " creation", "Number of parents and number of parent's weight should be equal")
        if len(self.state_names) != len(self.state_values):
            raise NodeInvalidStateError("At: " + self.title + " creation", "State names and state values should have the same length")
        
    def add_parent(self, parent_node, weight):
        self.parents.append(parent_node)
        self.parents_weight.append(weight)
        parent_node.__add_child(self)
        self.has_changed()

    def __add_child(self, child_node):
        self.children.append(child_node)

    def print_parents(self):
        print("Parents of {}: ".format(self.title))
        for p in self.parents:
            print(p.title)

    def has_changed(self):
        self.npt.init()

    def __repr__(self):
        return self.title