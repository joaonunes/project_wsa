from node import Node
from wsa import WSA
from util import CSV

# 3 estados 
# Child
a = Node('CKND_12_30', ["x2_MG_L", "x4_MG_L", "x6_MG_L"])

# Parents
b = Node('CKNI_12_15', ["x20_MG_L", "x30_MG_L", "x40_MG_L"])
c = Node('CKND_12_15', ["x2_MG_L", "x4_MG_L", "x6_MG_L"])
d = Node('CKNN_12_15', ["x0_5_MG_L", "x1_MG_L", "x2_MG_L"])

a.add_parent(b, 0.005)
a.add_parent(c, 0.99)
a.add_parent(d, 0.005)

a.npt.add_compatible([0, 1, 2], [.0, .9683, .0317], 0)
a.npt.add_compatible([1, 2, 0], [.0, .0, 1], 0)
a.npt.add_compatible([2, 1, 1], [.0, .9048, .0952], 0)

a.npt.add_compatible([0, 0, 0], [.9524, .0476, 0], 1)
a.npt.add_compatible([2, 1, 2], [.0, .8889, .1111], 1)
a.npt.add_compatible([1, 2, 1], [.0, .0, 1], 1)

a.npt.add_compatible([2, 1, 0], [.0, .9127, 0.0873], 2)
a.npt.add_compatible([2, 2, 1], [.0, .0, 1], 2)
a.npt.add_compatible([2, 1, 2], [.0, .8889, .1111], 2)

WSA.apply(a)
# CSV.write("temp.csv", a.npt)