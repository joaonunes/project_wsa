from exceptions import NodeInvalidStateError

class WSA:
    def __init__(self):
        pass

    @staticmethod
    def apply(node):
        """
        Gera o restante da tabela a partir das combinações de pais compatíveis.

        node - nó em que será aplicado o WSA
        """

        # tabela que vamos alterar
        npt = node.npt  
        
        # why .99999999...? 
        # .3+.6+.1 == .9999999999999999 not 1
        if sum(npt.child.parents_weight) < .99:
            raise NodeInvalidStateError("At: Applying WSA", "The sum of weights should be 1")

        # Gera a distribuição para cada combinação de pais usando uma soma ponderada
        # entry = distribuição probabilística de uma certa combinação de pais
        # A NPT é composta por n objetos do tipo Entry
        for entry in npt.distributions:
            #  Se a entrada for de uma combinação de pais compatíveis, ignora.
            if not entry.is_compatible: 
                # Dado um nó-filho A e os nós-pais B e C
                # Obtemos a probabilidade para cada estado de A
                # state_of_interest vai ser L, M e finalmente H (caso tenha 3 estados)  
                for state_of_interest in range (0, len(npt.child.state_values)):
                    res = 0  # Vai guardar a probabilidade resultante
                    for current_parent in range (0, len(npt.child.parents)):
                        # Realiza a soma ponderada
                        # 
                        # peso_b = 0.4
                        # peso_c = 0.6
                        #
                        # p(A = l | B = l, C = h) 
                        # = 
                        # peso_b * p(A = l | {Comp(B = l)}) + peso_c * p(A = l | {Comp(C = h)})
                        #
                        # {Comp(B = l)} = [l = 0.7, m = 0.3, h = 0]
                        # {Comp(C = h)} = [l = 0.2, m = 0.4, h = 0.4]
                        #   
                        # Neste caso, o estado l do nó-filho A é igual a soma ponderada da probabilidade que está no estado l nos pais compatíveis com B = l e C = h.
                        # Ou seja, para saber o valor de l para o nó-filho o seguinte cálculo deve ser realizado: .4 * .7 + .6 * .2  
                        res += npt.child.parents_weight[current_parent] * npt.compatible_parents[current_parent][entry.evidences_as_int[current_parent]][state_of_interest]                            
                    entry.state_values[state_of_interest] = res  # Guarda o resultado da soma ponderada