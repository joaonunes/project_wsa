from sklearn.metrics import brier_score_loss
#from sklearn.metrics.pairwise import euclidean_distances
from scipy.spatial.distance import euclidean
from scipy.stats import entropy
import numpy as np


# Just to be sure =)
def bs(f, o):
    sum = 0
    for i in range (0, len(f)):
        sum += (f[i] - o[i])**2
    return sum / len(f)


# Caso B - Scenario 1 
print ("Scenario 1")
y_true_categorical = np.array(["very low", "low", "medium", "high", "very high"])
# WSA
y_true = np.array([0, 1, 0, 0, 0])
y_prob = np.array([0.133504, 0.34543785,0.3381815,0.1782792000000001,0.004597449999999999])
print("WSA (BrierScore): ", brier_score_loss(y_true, y_prob))
print("WSA (Euclidean Distance): ", euclidean(y_true, y_prob))
print("WSA (KL-devergence)", entropy(y_true, y_prob))

#RNM
y_prob = np.array([0.009404840464592902,0.201717218899692,0.5699035990927823,0.2105987057627672,0.008375635780165602])
print("RNM (BrierScore): ", brier_score_loss(y_true, y_prob))
print("RNM (Euclidean Distance): ", euclidean(y_true, y_prob))
print("RNM (KL-devergence)", entropy(y_true, y_prob))


# Caso B - Scenario 2 
print ("\nScenario 2")
y_true_categorical = np.array(["very low", "low", "medium", "high", "very high"])
# WSA
y_true = np.array([0, 0, 1, 0, 0])
y_prob = np.array([0.02201600000000001,0.4263252,0.4837272000000001,0.06758098749999999,0.0003506125])
print("WSA (BrierScore): ", brier_score_loss(y_true, y_prob))
print("WSA (Euclidean Distance): ", euclidean(y_true, y_prob))
print("WSA (KL-devergence)", entropy(y_true, y_prob))

#RNM
y_prob = np.array([0.0009041684518885999,0.06836824628855538,0.4679346666323676,0.4193792183303205,0.04341370029686781])
print("RNM (BrierScore): ", brier_score_loss(y_true, y_prob))
print("RNM (Euclidean Distance): ", euclidean(y_true, y_prob))
print("RNM (KL-devergence)", entropy(y_true, y_prob))


# Caso B - Scenario 3 
print ("\nScenario 3")
y_true_categorical = np.array(["very low", "low", "medium", "high", "very high"])
# WSA
y_true = np.array([0, 0, 1, 0, 0])
y_prob = np.array([0,0.04264450000000002,0.48811425,0.46315625,0.006085000000000002])
print("WSA (BrierScore): ", brier_score_loss(y_true, y_prob))
print("WSA (Euclidean Distance): ", euclidean(y_true, y_prob))
print("WSA (KL-devergence)", entropy(y_true, y_prob))

#RNM
y_prob = np.array([0.0021122142344643,0.1098546718454068,0.5297498422749822,0.3308943119068284,0.02738895973831831])
print("RNM (BrierScore): ", brier_score_loss(y_true, y_prob))
print("RNM (Euclidean Distance): ", euclidean(y_true, y_prob))
print("RNM (KL-devergence)", entropy(y_true, y_prob))


# Caso B - Scenario 4 
print ("\nScenario 4")
y_true_categorical = np.array(["very low", "low", "medium", "high", "very high"])
# WSA
y_true = np.array([0, 0, 0, 1, 0])
y_prob = np.array([0,0.07278351250000001,0.491025675,0.4361908125,0])
print("WSA (BrierScore): ", brier_score_loss(y_true, y_prob))
print("WSA (Euclidean Distance): ", euclidean(y_true, y_prob))
print("WSA (KL-devergence)", entropy(y_true, y_prob))

#RNM
y_prob = np.array([9.8808562737e-05,0.01942704468736961,0.296610984871143,0.5486673082261551,0.1351958536525955])
print("RNM (BrierScore): ", brier_score_loss(y_true, y_prob))
print("RNM (Euclidean Distance): ", euclidean(y_true, y_prob))
print("RNM (KL-devergence)", entropy(y_true, y_prob))


# Caso B - Scenario 5 
print ("\nScenario 5")
y_true_categorical = np.array(["very low", "low", "medium", "high", "very high"])
# WSA
y_true = np.array([0, 1, 0, 0, 0])
y_prob = np.array([0.16,0.4306875000000001,0.3446062499999999,0.06410625,0.0006000000000000001])
print("WSA (BrierScore): ", brier_score_loss(y_true, y_prob))
print("WSA (Euclidean Distance): ", euclidean(y_true, y_prob))
print("WSA (KL-devergence)", entropy(y_true, y_prob))

#RNM
y_prob = np.array([0.003662042542024001,0.1358446300108836,0.5657624866195033,0.2806765972285487,0.01405424359904041])
print("RNM (BrierScore): ", brier_score_loss(y_true, y_prob))
print("RNM (Euclidean Distance): ", euclidean(y_true, y_prob))
print("RNM (KL-devergence)", entropy(y_true, y_prob))

# Caso B - Scenario 6 
print ("\nScenario 6")
y_true_categorical = np.array(["very low", "low", "medium", "high", "very high"])
# WSA
y_true = np.array([0, 0, 0, 1, 0])
y_prob = np.array([0,0.0784336375,0.469508675,0.4403193375,0.01173835000000001])
print("WSA (BrierScore): ", brier_score_loss(y_true, y_prob))
print("WSA (Euclidean Distance): ", euclidean(y_true, y_prob))
print("WSA (KL-devergence)", entropy(y_true, y_prob))

#RNM
y_prob = np.array([5.167800000000001e-12,4.68650108067e-05,0.013629205297872,0.2967463752970337,0.6895775543891198])
print("RNM (BrierScore): ", brier_score_loss(y_true, y_prob))
print("RNM (Euclidean Distance): ", euclidean(y_true, y_prob))
print("RNM (KL-devergence)", entropy(y_true, y_prob))


# Caso B - Scenario 7 
print ("\nScenario 7")
y_true_categorical = np.array(["very low", "low", "medium", "high", "very high"])
# WSA
y_true = np.array([0, 0, 1, 0, 0])
y_prob = np.array([0.02000000000000001,0.24902,0.62576,0.10252,0.0027])
print("WSA (BrierScore): ", brier_score_loss(y_true, y_prob))
print("WSA (Euclidean Distance): ", euclidean(y_true, y_prob))
print("WSA (KL-devergence)", entropy(y_true, y_prob))

#RNM
y_prob = np.array([0.1119007287367197,0.5137584803359885,0.345175759334344,0.02896647053295931,0.0001985610599886])
print("RNM (BrierScore): ", brier_score_loss(y_true, y_prob))
print("RNM (Euclidean Distance): ", euclidean(y_true, y_prob))
print("RNM (KL-devergence)", entropy(y_true, y_prob))